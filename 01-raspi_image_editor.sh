#!/usr/bin/env bash

print('##################################################\n########### Raspberry Pi Image Editor ############\n##################################################')
echo ""

if [ -z "$1" ]
then
  echo "   1 = hostname"
  echo "   2 = path to boot partition"
  echo "   3 = path to system partition"
  exit 0
fi

file_dir="files"

wpa_file="$file_dir/wpa_supplicant.conf"
interfaces_file="$file_dir/interfaces"
hostname="$1"
path_boot="$2"
path_system="$3"

if [ ! -f "$path_boot/config.txt" ]
then
  echo "ERROR! Not a boot partition"
  exit 0
fi

if [ ! -f "$path_system/etc/fstab" ]
then
  echo "ERROR! Not a sytem partition"
  exit 0
fi

# Editing hostname
echo $hostname > $file_dir/hostname

sed -i '$d' $file_dir/hosts
echo "127.0.1.1		$hostname" >> $file_dir/hosts

# Copying files
cp hosts "$path_system/etc/hosts"
cp hostname "$path_system/etc/hostname"
cp $file_dir/$wpa_file "$path_boot"
touch $path_boot/ssh
# chmod 644 "$path_system/etc/wpa_supplicant/$wpa_file"
# cp $interfaces_file "$path_system/etc/network/$interfaces_file"
# chmod 644 "$path_system/etc/network/$interfaces_file"
