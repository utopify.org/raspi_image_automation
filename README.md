# Raspberry Pi Image Modification Automation

Modify \*.img fast doing following automatically:
- update **wpa_supplicant.conf** to have a wifi connection ready on first startup
- upload monitoring scripts like **cputemp.sh** to monitor the temperature
- install **cronjobs**, which will be triggered on every boot or every x hours
- edit the **hostname** before starting the operation system the first time

After modification the img file can be flashed on an sd card and is ready for a Raspberry Pi

## Why do I need this software?

I've configured a bunch of *Raspberry Pis* to crunch for [BOINC](https://utopify.org/boinc-what-is-it.html). This is a time consuming task and it was faster to create scripts, which are doing the important stuff automatically. 

The important steps:

- change **hosts** and **hostname**
- update **wpa_supplicant.conf** to connect to my router automatically after booting
- activate **ssh**
- change **password** on Raspberry Pi
- make my host trustworthy to connect to the Raspi without a password
- set my **time zonen**
- install predefined **cronjobs** to monitor cpu temperature
- install predefined **cronjobs** to deactivate unneeded resources on startup
- update the operating system
- install **vim** and **boinc**
- make my host able to connect with **boincmgr**
- restart **boinc service** to be able to connect to it and configure it with [BAM!](https://www.boincstats.com/bam) (a boinc account manager)

And that's it! With this tools I got a ready-to-go system within seconds.

## How to use it?

1. Download the headless (no GUI) version of [Raspberry Pi OS](https://www.raspberrypi.com/software/operating-systems/), which is called *Raspberry Pi OS Lite* or similar Debian based distributions.
2. Mount the two partitions of the image file where you want to, but you have to remember the paths (you need root privileges)
```
sudo losetup -P /dev/loop0p1 2021-01-11-raspios-buster-armhf-lite.img
sudo mount /dev/loop1p1 ~/mnt/image1
sudo mount /dev/loop1p2 ~/mnt/image2
```
3. Execute **01-raspi_image_editor.sh** with the paths of the **partitions** and your chosen **hostname**
```
sudo ./raspi_image_editor.sh raspi3no5 ~/mnt/image1 ~/mnt/image2
```
4. Unmount partitions and release the loop device 
```
sudo umount ~/mnt/image1 
sudo umount ~/mnt/image2
sudo losetup -d /dev/loop1
```
5. Flash the new image on your sd card and trigger a notification if it's done. ATTENTION!!! Change sdXXX to the one your sd card has. You can find it out with the following command: `lsblk`
```
sudo dd if=/home/maxmoon/os_images/2021-01-11-raspios-buster-armhf-lite.img of=/dev/sdXXX conv=fsync bs=4M && notify-send "Raspi is ready" || notify-send "ERROR" -u critical
```
6. Insert sd card into the Raspi and power it on
7. Execute **02-raspi_pusher.sh** and follow the instructions

*Congratulations! Your Raspi is ready!*

After that you can connect with **ssh** or **boincmgr** from your host system to your raspi and control the BOINC tasks.
