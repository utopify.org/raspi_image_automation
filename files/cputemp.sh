#!/usr/bin/env bash

# epoch
time=$(date +%s)

# Load
load1=$(cat /proc/loadavg | awk '{print $1}')

# Temperatur
cpuTT=$(cat /sys/class/thermal/thermal_zone0/temp | awk '{printf "%.1f", $1/1000}')

#Ausgabe 
echo $time';'$load1';'$cpuTT

