#!/usr/bin/env bash

echo "Raspi pusher"
echo ""

if [ -z "$1" ]
then
  echo "   1 = hostname"
  exit 0
fi

file_dir="files"
user="pi"
hostname="$1"
ip=$(hostname -I)
ssh-copy-id -i $HOME/.ssh/id_rsa.pub $user@$hostname
ssh $user@$hostname -C "sudo passwd $user"
ssh $user@$hostname -C "mkdir ~/bin ~/tmp"
scp $file_dir/cputemp.sh $user@$hostname:~/bin/
ssh $user@$hostname -C "sudo timedatectl set-timezone Europe/Zurich"

### uncomment to deactivate unneeded resources on startup (perfect for BOINC)
# scp $file_dir/raspicron $user@$hostname:~
# ssh $user@$hostname -C "crontab ~/raspicron && rm ~/raspicron"
# ssh $user@$hostname -C "cat /etc/hostname; ~/bin/cputemp.sh"
# ssh $user@$hostname -C "echo 0 | sudo tee /sys/devices/platform/soc/3f980000.usb/buspower"
# ssh $user@$hostname -C "sudo tvservice --off"

ssh $user@$hostname -C "sudo apt update && sudo apt upgrade -y"
ssh $user@$hostname -C "sudo apt install vim-nox boinc-client -y"
ssh $user@$hostname -C "echo $ip | sudo tee -a /etc/boinc-client/remote_hosts.cfg"
ssh $user@$hostname -C "sudo systemctl restart boinc-client"
notify-send "Raspi is ready"
